package uz.credit.appcreditshop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "products")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @Column(nullable = false,name = "name")
//    private String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "category_id")
    private Category category;
    @Column(updatable = false,insertable = false)
    private Long category_id;

    @Column(nullable = false,name = "price")
    private Long price;


}
