package uz.credit.appcreditshop.entity;

import lombok.*;
import uz.credit.appcreditshop.entity.address.Address;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "branch")
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "name")
    private String name;

    @Column(nullable = false,name = "active")
    private Boolean active;

    @OneToOne()
    @JoinColumn(name = "address_id",unique = true)
    private Address addressId;
    @Column(insertable = false,updatable = false)
    private Long address_id;

}
