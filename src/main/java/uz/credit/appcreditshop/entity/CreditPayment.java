package uz.credit.appcreditshop.entity;


import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "credit_payment")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreditPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "users_id")
    Users users;
//    @Column(insertable = false,updatable = false)
//    private Long users_id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    Products product;
    @Column(insertable = false,updatable = false)
    private Long product_id;


    @Column(nullable = false,name = "remaining_amount")
    Long remainingAmount;

    @Column(nullable = false,name = "monthly_payment")
    Long monthlyPayment;

//    @Column(nullable = false,name = "period")
//    Integer period;

    @Column(nullable = false,name ="payment_terms" )
    LocalDate paymentTerms=LocalDate.now();


    @Column(nullable = false,name = "active")
    Boolean active=true;
}
