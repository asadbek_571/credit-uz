package uz.credit.appcreditshop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Table(name = "employee")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "users_id")
    private Users usersId;
    @Column(insertable = false,updatable = false)
    private Long users_id;

//    @Column(nullable = false ,name = "phone_number")
//    private String phoneNumber;

    @OneToMany
    @JoinColumn(name = "positions_id")
    private List<Positions> positions;

    @Column(nullable = false, name = "active")
    private Boolean active;


}
