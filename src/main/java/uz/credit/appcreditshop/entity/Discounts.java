package uz.credit.appcreditshop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Table(name = "discounts")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Discounts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "current_price")
    private Double currentPrice;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Products product;
    @Column(insertable = false,updatable = false)
    private Long product_id;

}
