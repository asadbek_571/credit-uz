package uz.credit.appcreditshop.entity;

import com.sun.istack.NotNull;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDate;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "cash_payment")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CashPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "users_id")
    Users users;
    @Column(insertable = false, updatable = false)
    private Long users_id;

    @NotNull
    Double payment;

    @ManyToOne
    @JoinColumn(name = "product_id")
    Products products;
    @Column(updatable = false, insertable = false)
    Long product_id;

    @NotNull
    LocalDate paymentTime = LocalDate.now();


}
