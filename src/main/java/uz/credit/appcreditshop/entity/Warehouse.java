package uz.credit.appcreditshop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "warehouse")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Warehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "product_id")
    private Products product;
    @Column(insertable = false,updatable = false)
    private Long product_id;

    @Column(nullable = false,name = "active")
    private Boolean active;

    @Column(nullable = false,name = "category_name")
    String categoryName;

    @Column(nullable = false,name = "amount")
    Integer amount;


}
