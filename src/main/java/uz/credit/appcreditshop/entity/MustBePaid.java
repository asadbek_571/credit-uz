package uz.credit.appcreditshop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Table(name = "must_be_paid")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class MustBePaid {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @ManyToOne(optional = false)
//    @JoinColumn(name = "users_id")
//    private Users usersId;
//    @Column(insertable = false, updatable = false)
//    private Long users_id;

    @OneToMany
    private List<CreditPayment> creditPayment;


}
