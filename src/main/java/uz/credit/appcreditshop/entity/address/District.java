package uz.credit.appcreditshop.entity.address;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "district")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "name")
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "region_id")
    private Region region;
}
