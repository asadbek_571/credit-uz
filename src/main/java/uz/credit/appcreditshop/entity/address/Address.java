package uz.credit.appcreditshop.entity.address;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "street")
    private String street;

    @Column(nullable = false,name = "home_number")
    private String homeNumber;

    @ManyToOne(optional = false)
    @JoinColumn(name = "district_id")
    private District district;

}
