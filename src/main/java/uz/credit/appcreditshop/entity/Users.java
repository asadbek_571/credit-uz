package uz.credit.appcreditshop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.credit.appcreditshop.model.UserDto;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,name = "first_name")
    private String firstName;

    @Column(nullable = false,name = "last_name")
    private String lastName;

    @Column(name = "birthday")
    private LocalDate birthday;

    @Column(nullable = false,name = "passport_serial",unique = true)
    private String passportSerial;

    @Column(nullable = false,name = "phone_number",unique = true)
    private String phoneNumber;

    @Column(nullable = false,name = "address")
    private String address;

    public static Users toUser(Users users, UserDto dto) {
        users.setFirstName(dto.getFirstName() != null ? dto.getFirstName() : users.getFirstName());
        users.setLastName(dto.getLastName() != null ? dto.getLastName() : users.getLastName());
        users.setPassportSerial(dto.getPassportSerial() != null ? dto.getPassportSerial() : users.getPassportSerial());
        users.setAddress(dto.getAddress() != null ? dto.getAddress() : users.getAddress());
        users.setBirthday(dto.getBirthday() != null ? LocalDate.parse(dto.getBirthday(), DateTimeFormatter.ofPattern("yyyy-MMM-dd")) : users.getBirthday());
        users.setPhoneNumber(dto.getPhoneNumber() != null ? dto.getPhoneNumber() : users.getPhoneNumber());
        return users;
    }
}
