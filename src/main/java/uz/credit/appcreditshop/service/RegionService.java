package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.address.RegionDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface RegionService {
    ResponseEntity<ApiResponse<RegionDto>> add(RegionDto dto);

    ResponseEntity<ApiResponse<RegionDto>> update(Long id, RegionDto dto);
}
