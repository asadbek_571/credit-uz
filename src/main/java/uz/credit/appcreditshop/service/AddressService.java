package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.address.AddressDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface AddressService {
    ResponseEntity<ApiResponse<AddressDto>> add(AddressDto dto);

    ResponseEntity<ApiResponse<AddressDto>> update(Long id,AddressDto dto);
}
