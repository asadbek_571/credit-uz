package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.entity.CreditPayment;
import uz.credit.appcreditshop.model.CreditGetDto;
import uz.credit.appcreditshop.model.CreditPaymentCrudDto;
import uz.credit.appcreditshop.model.CreditPaymentDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

import java.util.List;

public interface CreditPaymentService {



    ResponseEntity<ApiResponse<List<CreditPayment>>> get(CreditGetDto dto);

    ResponseEntity<ApiResponse<CreditPaymentDto>> update(Long id, CreditPaymentCrudDto dto);
}
