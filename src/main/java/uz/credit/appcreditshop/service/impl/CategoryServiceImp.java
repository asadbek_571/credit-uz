package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.common.MapstructMapper;
import uz.credit.appcreditshop.entity.Category;
import uz.credit.appcreditshop.entity.Products;
import uz.credit.appcreditshop.model.CategoryDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.repository.CategoryRepo;
import uz.credit.appcreditshop.service.CategoryService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryServiceImp implements CategoryService {

    private final CategoryRepo categoryRepo;
    private final MapstructMapper mapstructMapper;

    @Override
    public ResponseEntity<ApiResponse<CategoryDto>> add(CategoryDto dto) {
        Optional<Category> optionalCategory = categoryRepo.findById(dto.getParentCategoryId());
        if (!optionalCategory.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Category not found"), HttpStatus.NOT_FOUND);

        Category category = mapstructMapper.toCategory(dto);

        categoryRepo.save(category);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<CategoryDto>> update(Long id, CategoryDto dto) {
        return null;
    }
}
