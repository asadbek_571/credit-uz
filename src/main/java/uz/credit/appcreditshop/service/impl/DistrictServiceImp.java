package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.model.address.DistrictDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.DistrictService;

@Service
@RequiredArgsConstructor
public class DistrictServiceImp implements DistrictService {
    @Override
    public ResponseEntity<ApiResponse<DistrictDto>> add(DistrictDto dto) {
        return null;
    }

    @Override
    public ResponseEntity<ApiResponse<DistrictDto>> update(Long id, DistrictDto dto) {
        return null;
    }
}
