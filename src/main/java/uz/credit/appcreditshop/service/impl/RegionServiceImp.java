package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.model.address.RegionDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.RegionService;

@Service
@RequiredArgsConstructor
public class RegionServiceImp implements RegionService {
    @Override
    public ResponseEntity<ApiResponse<RegionDto>> add(RegionDto dto) {
        return null;
    }

    @Override
    public ResponseEntity<ApiResponse<RegionDto>> update(Long id, RegionDto dto) {
        return null;
    }
}
