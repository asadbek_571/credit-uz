package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.model.ClientDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.ClientService;

@Service
@RequiredArgsConstructor
public class ClientServiceImp implements ClientService {
    @Override
    public ResponseEntity<ApiResponse<ClientDto>> add(ClientDto dto) {
        return null;
    }

    @Override
    public ResponseEntity<ApiResponse<ClientDto>> update(Long id, ClientDto dto) {
        return null;
    }
}
