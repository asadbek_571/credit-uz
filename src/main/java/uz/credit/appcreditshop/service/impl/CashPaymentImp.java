package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.common.MapstructMapper;
import uz.credit.appcreditshop.entity.*;
import uz.credit.appcreditshop.entity.address.Address;
import uz.credit.appcreditshop.entity.address.District;
import uz.credit.appcreditshop.model.CashPaymentDto;
import uz.credit.appcreditshop.model.CreditPaymentDto;
import uz.credit.appcreditshop.model.address.AddressDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.repository.*;
import uz.credit.appcreditshop.service.AddressService;
import uz.credit.appcreditshop.service.CashPaymentService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CashPaymentImp implements CashPaymentService {

    private final CashPaymentRepo cashPaymentRepo;
    private final CreditPaymentRepo creditPaymentRepo;
    private final ProductRepo productRepo;
    private final UserRepo userRepo;
    private final WarehouseRepo warehouseRepo;
    private final MapstructMapper mapstructMapper;


    @Override
    public ResponseEntity<ApiResponse<CashPaymentDto>> update(Long id, CashPaymentDto dto) {
        return null;
    }

    @Override
    public ResponseEntity<ApiResponse<CashPaymentDto>> add(CashPaymentDto dto, int term) {

//        todo Nechta mahsulot sotib olishini qo'shaman

        Optional<Users> usersOptional = userRepo.findById(dto.getUsersId());
        if (!usersOptional.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("User not found"), HttpStatus.NOT_FOUND);

        Optional<Products> productsOptional = productRepo.findById(dto.getProductId());

        if (!productsOptional.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Product not found"), HttpStatus.NOT_FOUND);

        Products products = productsOptional.get();
        List<Warehouse> optionalWarehouse = warehouseRepo.getWarehouseByActiveTrueAndProduct_id(products.getId());

        if (dto.getPayment() < products.getPrice()) {
            int counter = 0;
            if (term == 0) {
                return new ResponseEntity<>(new ApiResponse<>("Payment term not included"), HttpStatus.BAD_REQUEST);
            }
            while (counter < term) {
//            todo boshlang'ich to'lov qilishi shart
                if (dto.getPayment() <= 0) {
                    return new ResponseEntity<>(new ApiResponse<>("Payment not made"), HttpStatus.BAD_REQUEST);
                }
                Long amountPaid = dto.getPayment();
                Long price = products.getPrice();
                Long addedInterest;
                Long monthlyPayment;

                LocalDate nowDate = LocalDate.now().plusMonths(counter);

                CreditPaymentDto creditPaymentDto = new CreditPaymentDto();
                creditPaymentDto.setUsersId(dto.getUsersId());
                creditPaymentDto.setPaymentTerms(nowDate);

                addedInterest = price;
//              todo foyizni olish
                addedInterest /= 100;
                addedInterest *= 10;

//              todo qolgan summa
                Long v = price - amountPaid + addedInterest;

//              todo oylik to'lov
                addedInterest += price;
                monthlyPayment = addedInterest;
                monthlyPayment /= term;

                creditPaymentDto.setRemainingAmount(v);
                creditPaymentDto.setMonthlyPayment(monthlyPayment);
                creditPaymentDto.setProductId(dto.getProductId());
                CreditPayment creditPayment = mapstructMapper.toCreditPayment(creditPaymentDto);
                creditPaymentRepo.save(creditPayment);

                counter++;

            }
            return new ResponseEntity<>(new ApiResponse<>("Credit details saved"), HttpStatus.OK);
        }
        CashPayment cashPayment = mapstructMapper.toCashPayment(dto);
        cashPaymentRepo.save(cashPayment);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);

    }

}
