package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.common.MapstructMapper;
import uz.credit.appcreditshop.entity.Category;
import uz.credit.appcreditshop.entity.Products;
import uz.credit.appcreditshop.entity.Warehouse;
import uz.credit.appcreditshop.model.WarehouseDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.repository.CategoryRepo;
import uz.credit.appcreditshop.repository.ProductRepo;
import uz.credit.appcreditshop.repository.WarehouseRepo;
import uz.credit.appcreditshop.service.WarehouseService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WarehouseServiceImp implements WarehouseService {

    private final WarehouseRepo warehouseRepo;

    private final MapstructMapper mapstructMapper;

    private final CategoryRepo categoryRepo;

    private final ProductRepo productRepo;

    @Override
    public ResponseEntity<ApiResponse<WarehouseDto>> add(WarehouseDto dto) {

        Optional<List<Category>> optionalCategories = categoryRepo.findAllByName(dto.getCategoryName());
        if (!optionalCategories.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Category not found"), HttpStatus.NOT_FOUND);
        Warehouse warehouse = mapstructMapper.toWarehouse(dto);

        warehouseRepo.save(warehouse);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);
    }
    @Override
    public ResponseEntity<ApiResponse<List<Warehouse>>> getList(Long productId, Integer page, Integer size) {
        List<Long> allById = warehouseRepo.warehouseId(productId);

        if (allById.isEmpty())
            return new ResponseEntity<>(new ApiResponse<>("Product not found"), HttpStatus.NOT_FOUND);
        List<Warehouse> allBy= warehouseRepo.getList(allById, page * size, size);

        return new ResponseEntity<>(new ApiResponse<>(allBy), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<WarehouseDto>> update(Long id, WarehouseDto dto) {
        return null;
    }
}
