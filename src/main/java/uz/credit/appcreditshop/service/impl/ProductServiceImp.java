package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.common.MapstructMapper;
import uz.credit.appcreditshop.entity.Category;
import uz.credit.appcreditshop.entity.Products;
import uz.credit.appcreditshop.entity.address.Address;
import uz.credit.appcreditshop.entity.address.District;
import uz.credit.appcreditshop.model.ProductDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.repository.CategoryRepo;
import uz.credit.appcreditshop.repository.ProductRepo;
import uz.credit.appcreditshop.service.ProductService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImp implements ProductService {

    private final ProductRepo productRepo;
    private final CategoryRepo categoryRepo;
    private final MapstructMapper mapstructMapper;

    @Override
    public ResponseEntity<ApiResponse<ProductDto>> add(ProductDto dto) {
        Optional<Category> optionalCategory = categoryRepo.findById(dto.getCategoryId());
        if (!optionalCategory.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Category not found"), HttpStatus.NOT_FOUND);

        Products products = mapstructMapper.toProducts(dto);
        productRepo.save(products);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<ProductDto>> update(Long id, ProductDto dto) {

        Optional<Products> optionalProducts = productRepo.findById(id);
        if (!optionalProducts.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Product not found"), HttpStatus.NOT_FOUND);

        Optional<Category> optionalCategory = categoryRepo.findById(dto.getCategoryId());
        if (!optionalCategory.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Category not found"), HttpStatus.NOT_FOUND);

        Products products = optionalProducts.get();
        products.setCategory(optionalCategory.get());
        products.setPrice(dto.getPrice());
//        products.setName(dto.getName());
        productRepo.save(products);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);
    }
}
