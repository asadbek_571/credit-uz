package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.common.MapstructMapper;
import uz.credit.appcreditshop.entity.Users;
import uz.credit.appcreditshop.helpers.Utils;
import uz.credit.appcreditshop.model.UserDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.repository.UserRepo;
import uz.credit.appcreditshop.service.UserService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final MapstructMapper mapstructMapper;


    @Override
    public ResponseEntity<ApiResponse<UserDto>> add(UserDto dto) {
        if (Utils.isEmpty(dto))
            return new ResponseEntity<>(new ApiResponse<>("The data is incomplete"), HttpStatus.BAD_REQUEST);

        Optional<Users> userByPassportSerial = userRepo.getUserByPassportSerial(dto.getPassportSerial());
        if (userByPassportSerial.isPresent()){
            return new ResponseEntity<>(new ApiResponse<>("Available in the users database"), HttpStatus.BAD_REQUEST);
        }

        Users users = mapstructMapper.toUser(dto);
        userRepo.save(users);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<UserDto>> update(Long id, UserDto dto) {
        Optional<Users> optionalUser = userRepo.findById(id);
        if (!optionalUser.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Users not found"), HttpStatus.NOT_FOUND);

        if (Utils.isEmpty(dto))
            return new ResponseEntity<>(new ApiResponse<>("The data is incomplete"), HttpStatus.BAD_REQUEST);

        Users users = Users.toUser(optionalUser.get(), dto);

        userRepo.save(users);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);
    }

}
