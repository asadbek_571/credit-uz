package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.common.MapstructMapper;
import uz.credit.appcreditshop.entity.address.Address;
import uz.credit.appcreditshop.entity.address.District;
import uz.credit.appcreditshop.model.address.AddressDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.repository.AddressRepo;
import uz.credit.appcreditshop.repository.DistrictRepo;
import uz.credit.appcreditshop.service.AddressService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressServiceImp implements AddressService {

    private final AddressRepo addressRepo;
    private final DistrictRepo districtRepo;
    private final MapstructMapper mapstructMapper;

    @Override
    public ResponseEntity<ApiResponse<AddressDto>> add(AddressDto dto) {
        Optional<District> optionalDistrict = districtRepo.findById(dto.getDistrictId());
        if (!optionalDistrict.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("District not found"), HttpStatus.NOT_FOUND);

        Address address = mapstructMapper.toAddress(dto);
        addressRepo.save(address);
        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);

    }

    @Override
    public ResponseEntity<ApiResponse<AddressDto>> update(Long id, AddressDto dto) {

        Optional<Address> optionalAddress = addressRepo.findById(id);
        if (!optionalAddress.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("Address not found"), HttpStatus.NOT_FOUND);

        Optional<District> optionalDistrict = districtRepo.findById(dto.getDistrictId());
        if (!optionalDistrict.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("District not found"), HttpStatus.NOT_FOUND);

        Address address = optionalAddress.get();
        address.setDistrict(optionalDistrict.get());
        address.setHomeNumber(dto.getHomeNumber());
        address.setStreet(dto.getStreet());
        addressRepo.save(address);

        return new ResponseEntity<>(new ApiResponse<>(dto), HttpStatus.OK);
    }
}
