package uz.credit.appcreditshop.service.impl;

import lombok.RequiredArgsConstructor;
import net.bytebuddy.asm.Advice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.credit.appcreditshop.entity.CreditPayment;
import uz.credit.appcreditshop.entity.Warehouse;
import uz.credit.appcreditshop.model.CreditGetDto;
import uz.credit.appcreditshop.model.CreditPaymentCrudDto;
import uz.credit.appcreditshop.model.CreditPaymentDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.repository.CreditPaymentRepo;
import uz.credit.appcreditshop.service.CreditPaymentService;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class CreditPaymentImp implements CreditPaymentService {

    private final CreditPaymentRepo creditPaymentRepo;

    @Override
    public ResponseEntity<ApiResponse<CreditPaymentDto>> update(Long id, CreditPaymentCrudDto dto) {

        CreditPaymentImp creditPaymentImp = new CreditPaymentImp(creditPaymentRepo);


        LocalDate localDate = LocalDate.now();
        Optional<List<CreditPayment>> falseAndUsersId = creditPaymentRepo.findByActiveFalseAndUsersId(id);
        Optional<List<CreditPayment>> activeTrueAndUsersId = creditPaymentRepo.findAllByActiveTrueAndUsersId(id);
        if (!activeTrueAndUsersId.isPresent())
            return new ResponseEntity<>(new ApiResponse<>("There are no active fees"), HttpStatus.NOT_FOUND);

        List<CreditPayment> creditPayments = activeTrueAndUsersId.get();

        for (CreditPayment creditPayment : creditPayments) {


            if (dto.getPayment() == 0 || dto.getPayment() < creditPayment.getMonthlyPayment()) {
                return new ResponseEntity<>(new ApiResponse<>("The specified amount has not been paid"), HttpStatus.NOT_FOUND);
            }

//         todo   agar credit to'lovni hammasi to'lansa
            long l = dto.getPayment() - creditPayment.getRemainingAmount();
            if (dto.getPayment() > creditPayment.getRemainingAmount() || l == 0) {
                return creditPaymentImp.PayItAll(creditPayment, dto, activeTrueAndUsersId.get());
            }

//            if (dto.getPayment() > creditPayment.getMonthlyPayment()) {
////                long quantity = dto.getPayment() - creditPayment.getRemainingAmount();
////            todo active bo'lmasa qaytariladigan shummani ko'rsatish
////                if (!activeTrue.isPresent())
////                    return new ResponseEntity<>(new ApiResponse<>(String.format("Refundable amount %s", quantity)), HttpStatus.OK);
////
//                long quantity;
//                long l;
//                long a=0;
//                for (CreditPayment payment : activeTrueAndUsersId.get()) {
//                    quantity = dto.getPayment() - payment.getMonthlyPayment();
//                    l = dto.getPayment() - quantity;
//                    l-=a;
//                    long remainingAmount = 0;
//                    if (l >= payment.getMonthlyPayment()) {
//                        payment.setMonthlyPayment(l);
//                        if (falseAndUsersId.isPresent()) {
//                            for (int i = 0; i < falseAndUsersId.get().size(); i++) {
//                                Long lastElement = falseAndUsersId.get().get(i).getRemainingAmount();
//
//                                remainingAmount = lastElement - payment.getMonthlyPayment();
//                                payment.setRemainingAmount(remainingAmount);
//                            }
//                        }
//
//                    }else if (l<payment.getMonthlyPayment()&&l>0){
//                        if (falseAndUsersId.isPresent()) {
//                            for (int i = 0; i < falseAndUsersId.get().size(); i++) {
//                                Long lastElement = falseAndUsersId.get().get(i).getRemainingAmount();
//
//                                remainingAmount = lastElement - payment.getMonthlyPayment();
//                                payment.setRemainingAmount(remainingAmount);
//                            }
//                        }
//                      payment.setRemainingAmount((remainingAmount-l));
//                    }
//
//                    if (remainingAmount == 0) {
//                        payment.setActive(false);
//                        payment.setRemainingAmount((payment.getRemainingAmount() - dto.getPayment()));
//                    }
//                   a+=l;
//                    creditPaymentRepo.save(payment);
//
//                    Long monthlyPayment;
//                    if (dto.getPayment() > payment.getMonthlyPayment()) {
//
//                        monthlyPayment = paymentTerms.get().getMonthlyPayment() - dto.getPayment();
//                        Long v = (dto.getPayment() - monthlyPayment);
//                        long remainingAmount = payment.getRemainingAmount() - v;
//                        payment.setRemainingAmount((remainingAmount));
//                        payment.setActive(false);
//                        creditPaymentRepo.save(payment);
//
//                        if (monthlyPayment >= payment.getMonthlyPayment()) {
//                            remainingAmount = payment.getRemainingAmount() - dto.getPayment();
//                            payment.setRemainingAmount(remainingAmount);
//                            payment.setActive(false);
//                            creditPaymentRepo.save(payment);
//                            break;
//                        } else
//                            return new ResponseEntity<>(new ApiResponse<>(String.format("Refundable amount %s", monthlyPayment)), HttpStatus.OK);
//                    }
//                    if (payment.getActive()) {
//                        Long remainingAmount = payment.getRemainingAmount() - dto.getPayment();
//                        payment.setRemainingAmount(remainingAmount);
//                        payment.setActive(false);
//                        creditPaymentRepo.save(payment);
//                        break;
//                    }

//            if (dto.getPayment() > creditPayment.getMonthlyPayment()) {
//                long sum = 0;
//                long counter = 0;
//                for (CreditPayment payment : activeTrueAndUsersId.get()) {
//                    long quantity = dto.getPayment() - creditPayment.getMonthlyPayment();
//                    Month month = payment.getPaymentTerms().getMonth().plus(counter);
//                    quantity -= sum;
//                    if (quantity < 0) {
//                        long abs = Math.abs(quantity);
//                        long monthlyPayment = payment.getMonthlyPayment() - abs;
//                        payment.setMonthlyPayment(monthlyPayment);
//                        if (falseAndUsersId.isPresent()) {
//                            int size = falseAndUsersId.get().size();
//
//                            if (size != 0) {
//                                Long lastElement = falseAndUsersId.get().get(falseAndUsersId.get().size() - 1).getRemainingAmount();
//                                lastElement -= dto.getPayment();
//                                creditPayment.setRemainingAmount(lastElement);
//                            }
//                        }
//                        String format = String.format("For the month of %s ", month);
//                        creditPaymentRepo.save(payment);
//                        return new ResponseEntity<>(new ApiResponse<>(String.format(format + "  Less than %s sums", abs)), HttpStatus.OK);
//                    }
//
//                    long monthlyPayment = dto.getPayment() - quantity;
//                    sum += monthlyPayment;
//                    long amount;
//                    Optional<List<Long>> active = creditPaymentRepo.active();
//                    if (active.isPresent()) {
//                        List<Long> longs = active.get();
//                        if (longs.size() == 1 && longs.get(0) > 0) {
//                            Long aLong = longs.get(0);
//                            long previousValue = (monthlyPayment - aLong);
//                            amount = (monthlyPayment - previousValue);
//                            amount += aLong;
//                            payment.setMonthlyPayment(amount);
//                            payment.setActive(false);
//                            payment.setRemainingAmount((payment.getRemainingAmount() - dto.getPayment()));
//                            creditPaymentRepo.save(payment);
//                        }
//                    }
//                    payment.setActive(false);
////                        payment.setRemainingAmount((payment.getRemainingAmount() - dto.getPayment()));
//                    creditPaymentRepo.save(payment);
//
//                    if (falseAndUsersId.isPresent()) {
//                        if (falseAndUsersId.get().size() != 0) {
//                            Long lastElement = falseAndUsersId.get().get(falseAndUsersId.get().size() - 1).getRemainingAmount();
//                            lastElement -= dto.getPayment();
//                            creditPayment.setRemainingAmount(lastElement);
//
//                        }
//                    }
//                    creditPaymentRepo.save(payment);
//                }
//
////                return new ResponseEntity<>(new ApiResponse<>("Success"), HttpStatus.OK);
//            }
//            return new ResponseEntity<>(new ApiResponse<>("No previous payments"), HttpStatus.NOT_FOUND);

            if (dto.getPayment() > creditPayment.getMonthlyPayment()) {
                return creditPaymentImp.nextMonth(creditPayments, dto, falseAndUsersId);
            }

            if (localDate.getMonth().equals(creditPayment.getPaymentTerms().getMonth())) {
                Long remainingAmount = creditPayment.getRemainingAmount() - dto.getPayment();
                creditPayment.setRemainingAmount(remainingAmount);
                creditPayment.setActive(false);
                creditPaymentRepo.save(creditPayment);
                return new ResponseEntity<>(new ApiResponse<>("Success"), HttpStatus.OK);
            }
        }
        return null;
    }

    private ResponseEntity<ApiResponse<CreditPaymentDto>> nextMonth(List<CreditPayment> creditPayments, CreditPaymentCrudDto dto, Optional<List<CreditPayment>> falseAndUsersId) {
//        long counter = 0;
//        long sss = 0;
//        long z = 0;
//        for (CreditPayment creditPayment : creditPayments) {
//            long quantity = dto.getPayment() - creditPayment.getMonthlyPayment();
//            quantity -= z;
//            if (quantity > 0 && quantity >= creditPayment.getMonthlyPayment()) {
//                creditPayment.setMonthlyPayment(creditPayment.getMonthlyPayment());
//
//                z += creditPayment.getMonthlyPayment();
//            }
//        }
//            if (falseAndUsersId.isPresent()) {
//                List<CreditPayment> paymentList = falseAndUsersId.get();
//                if (!paymentList.isEmpty()) {
//                    Long monthlyPayment = falseAndUsersId.get().get(falseAndUsersId.get().size() - 1).getMonthlyPayment();
////                    todo eski oygaa kam to'langan bo'lsa shu to'lovni o'rnini qoplash
//                    long payment = (monthlyPayment + quantity);
//                    if (payment > creditPayment.getMonthlyPayment()) {
////                        todo agar eski oyga
//                        long a = (payment - creditPayment.getMonthlyPayment());
//                        a -= sss;
//                        if (a > creditPayment.getMonthlyPayment()) {
//                            sss += (a - creditPayment.getMonthlyPayment());
//                        }
//
//
//                    }
//                    Long remainingAmount = falseAndUsersId.get().get(falseAndUsersId.get().size() - 1).getRemainingAmount();
//
//                }
//
//            }


        return null;
    }

    private ResponseEntity<ApiResponse<CreditPaymentDto>> PayItAll(CreditPayment creditPayment, CreditPaymentCrudDto dto, List<CreditPayment> optionalCreditPayment) {
        boolean isTrue = false;
        String message = "";
        //                to'lov miqdori ko'p to'lansa
        if (creditPayment.getRemainingAmount() < dto.getPayment()) {
            long quantity = dto.getPayment() - creditPayment.getRemainingAmount();
            isTrue = true;
            message = String.format("Refundable amount %s", quantity);
        }

        for (CreditPayment payment : optionalCreditPayment) {
            payment.setActive(false);
            payment.setRemainingAmount(0L);
        }
        creditPaymentRepo.save(creditPayment);
        return new ResponseEntity<>(new ApiResponse<>(isTrue ? "Success " + message : "Success"), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<List<CreditPayment>>> get(CreditGetDto dto) {

        List<CreditPayment> creditPaymentList;
        if (dto.getLocalDate() != null)
            creditPaymentList = creditPaymentRepo.findAllByActiveAndPaymentTerms(true, dto.getLocalDate());
        else
            creditPaymentList = creditPaymentRepo.findAllByActiveAndPaymentTerms(true, LocalDate.now());

        if (creditPaymentList.isEmpty())
            return new ResponseEntity<>(new ApiResponse<>("Credit not found"), HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(new ApiResponse<>(creditPaymentList), HttpStatus.OK);
    }
}
