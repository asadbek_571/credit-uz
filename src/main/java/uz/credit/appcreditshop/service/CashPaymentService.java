package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.CashPaymentDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface CashPaymentService {
    ResponseEntity<ApiResponse<CashPaymentDto>> update(Long id, CashPaymentDto dto);

    ResponseEntity<ApiResponse<CashPaymentDto>> add(CashPaymentDto dto,int term);
}
