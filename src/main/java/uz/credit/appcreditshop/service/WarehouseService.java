package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.entity.Warehouse;
import uz.credit.appcreditshop.model.WarehouseDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

import java.util.List;

public interface WarehouseService {
    ResponseEntity<ApiResponse<WarehouseDto>> add(WarehouseDto dto);

    ResponseEntity<ApiResponse<WarehouseDto>> update(Long id, WarehouseDto dto);

    ResponseEntity<ApiResponse<List<Warehouse>>> getList(Long id, Integer page, Integer size);
}
