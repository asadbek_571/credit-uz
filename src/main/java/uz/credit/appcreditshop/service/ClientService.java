package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.ClientDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface ClientService {
    ResponseEntity<ApiResponse<ClientDto>> add(ClientDto dto);

    ResponseEntity<ApiResponse<ClientDto>> update(Long id, ClientDto dto);
}
