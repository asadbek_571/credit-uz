package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.EmployeeDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface EmployeeService {
    ResponseEntity<ApiResponse<EmployeeDto>> add(EmployeeDto dto);

    ResponseEntity<ApiResponse<EmployeeDto>> update(Long id, EmployeeDto dto);
}
