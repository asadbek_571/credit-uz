package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.ProductDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface ProductService {
    ResponseEntity<ApiResponse<ProductDto>> add(ProductDto dto);

    ResponseEntity<ApiResponse<ProductDto>> update(Long id, ProductDto dto);
}
