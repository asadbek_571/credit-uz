package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.address.DistrictDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface DistrictService {
    ResponseEntity<ApiResponse<DistrictDto>> add(DistrictDto dto);

    ResponseEntity<ApiResponse<DistrictDto>> update(Long id, DistrictDto dto);
}
