package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.UserDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface UserService {
    ResponseEntity<ApiResponse<UserDto>> add(UserDto dto);

    ResponseEntity<ApiResponse<UserDto>> update(Long id, UserDto dto);
}
