package uz.credit.appcreditshop.service;

import org.springframework.http.ResponseEntity;
import uz.credit.appcreditshop.model.CategoryDto;
import uz.credit.appcreditshop.model.response.ApiResponse;

public interface CategoryService {
    ResponseEntity<ApiResponse<CategoryDto>> add(CategoryDto dto);

    ResponseEntity<ApiResponse<CategoryDto>> update(Long id, CategoryDto dto);
}
