//package uz.credit.appcreditshop.service;
//
//import org.springframework.http.ResponseEntity;
//import uz.credit.appcreditshop.entity.CreditPayment;
//import uz.credit.appcreditshop.model.MustBePaidDto;
//import uz.credit.appcreditshop.model.MustBePaidGetDto;
//import uz.credit.appcreditshop.model.response.ApiResponse;
//
//import java.time.LocalDate;
//import java.util.List;
//
//public interface MustBePaidService {
//    ResponseEntity<ApiResponse<List<CreditPayment>>> get(MustBePaidGetDto dto);
//
//    ResponseEntity<ApiResponse<MustBePaidDto>> update(Long id, MustBePaidDto dto);
//}
