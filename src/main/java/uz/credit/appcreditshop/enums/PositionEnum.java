package uz.credit.appcreditshop.enums;

public enum PositionEnum {
    GUARD,
    CASHIER,
    SALESMAN,
    SUPPLIER,
    DIRECTOR,
}
