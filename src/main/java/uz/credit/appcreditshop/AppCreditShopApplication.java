package uz.credit.appcreditshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppCreditShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppCreditShopApplication.class, args);
    }

}
