package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.Products;

import java.util.List;

@Repository
public interface ProductRepo extends JpaRepository<Products,Long> {

    List<Long>findAllById(Long id);
}
