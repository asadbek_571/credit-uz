package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.CreditPayment;
import uz.credit.appcreditshop.entity.Users;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<Users,Long> {

    Optional<Users> getUserByPassportSerial(String passportSerial);

//    @Query(value = "select * from credit_payment\n" +
//            "where users_id=1",nativeQuery = true);



}
