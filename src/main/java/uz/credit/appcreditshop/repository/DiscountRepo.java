package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.Discounts;

@Repository
public interface DiscountRepo extends JpaRepository<Discounts,Long> {
}
