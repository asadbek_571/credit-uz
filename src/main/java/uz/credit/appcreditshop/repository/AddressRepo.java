package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.credit.appcreditshop.entity.address.Address;

import java.util.Optional;

public interface AddressRepo extends JpaRepository<Address,Long> {



}
