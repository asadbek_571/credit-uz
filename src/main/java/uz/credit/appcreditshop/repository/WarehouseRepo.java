package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.Warehouse;

import java.util.List;


@Repository
public interface WarehouseRepo extends JpaRepository<Warehouse,Long> {

    @Query(nativeQuery = true, value = " SELECT t FROM warehouse t " +
            "            WHERE t.active =true AND t.id IN (:warehouseId) " +
            "             LIMIT :size OFFSET :start ")
    List<Warehouse> getList(List<Long>  warehouseId, Integer start, Integer size);

    @Query(value = "SELECT t.id FROM warehouse t " +
            "             WHERE t.active = true " +
            "            AND t.product_id IN(:productId)",nativeQuery = true)
    List<Long>warehouseId(Long productId);

    List<Warehouse> getWarehouseByActiveTrueAndProduct_id(Long product_id);


}
