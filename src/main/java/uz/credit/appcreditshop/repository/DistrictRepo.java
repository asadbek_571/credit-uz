package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.credit.appcreditshop.entity.address.District;

public interface DistrictRepo extends JpaRepository<District,Long> {
}
