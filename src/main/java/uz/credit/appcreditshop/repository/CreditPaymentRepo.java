package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.credit.appcreditshop.entity.CreditPayment;
import uz.credit.appcreditshop.entity.address.Address;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CreditPaymentRepo extends JpaRepository<CreditPayment, Long> {


    List<CreditPayment> findAllByActiveAndPaymentTerms(Boolean active, LocalDate paymentTerms);


    Optional<CreditPayment> getByUsers_id(Long users_id);

    @Query(value = " select * from credit_payment " +
            " left join users u on u.id = credit_payment.users_id " +
            " where u.id=:users_id ",nativeQuery = true)
    Optional<List<CreditPayment>> findByUsersId(Long users_id);

    Optional<CreditPayment> findByActiveFalseAndPaymentTerms(LocalDate paymentTerms);
    
    @Query(value = "select * from credit_payment\n" +
            "where active=false and users_id=:users_id",nativeQuery = true)
    Optional<List<CreditPayment>> findByActiveFalseAndUsersId(Long users_id);
    
    Optional<List<CreditPayment>>findAllByActiveTrueAndUsersId(Long users_id);
    @Query(value = " select t.monthly_payment from credit_payment t" +
            " where active=true and monthly_payment =(select min(monthly_payment)from credit_payment)",nativeQuery = true)
    Optional<List<Long>> active();
    

    Optional<CreditPayment> findByUsers_PassportSerial(String users_passportSerial);

}
