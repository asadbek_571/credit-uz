package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.MustBePaid;

@Repository
public interface MustBePaidRepo extends JpaRepository<MustBePaid,Long> {
}
