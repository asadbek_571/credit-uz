package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.credit.appcreditshop.entity.CashPayment;
import uz.credit.appcreditshop.entity.address.Address;

public interface CashPaymentRepo extends JpaRepository<CashPayment,Long> {



}
