package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.Employee;


@Repository
public interface EmployeeRepo extends JpaRepository<Employee,Long> {
}
