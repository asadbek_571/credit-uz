package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.address.Region;

@Repository
public interface RegionRepo extends JpaRepository<Region,Long> {
}
