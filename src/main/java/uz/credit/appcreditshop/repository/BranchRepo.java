package uz.credit.appcreditshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.credit.appcreditshop.entity.Branch;

import java.util.Optional;


@Repository
public interface BranchRepo extends JpaRepository<Branch,Long> {



}
