package uz.credit.appcreditshop.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentsDto {

    String paymentTerm;

    Double paymentValue;

    Boolean active;
}
