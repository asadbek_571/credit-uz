package uz.credit.appcreditshop.model;

import lombok.*;
import lombok.experimental.FieldDefaults;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CashPaymentDto {


    Long usersId;

    Long productId;

    Long payment;

//    String paymentTime;

}
