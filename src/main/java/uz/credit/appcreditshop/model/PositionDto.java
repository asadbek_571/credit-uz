package uz.credit.appcreditshop.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.credit.appcreditshop.enums.PositionEnum;

import javax.persistence.Column;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PositionDto {

     String position;
}
