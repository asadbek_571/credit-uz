package uz.credit.appcreditshop.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.credit.appcreditshop.entity.Category;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductDto {

//     String name;

     Long categoryId;

     Long price;
}
