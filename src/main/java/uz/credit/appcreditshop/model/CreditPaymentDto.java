package uz.credit.appcreditshop.model;


import lombok.*;
import lombok.experimental.FieldDefaults;


import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreditPaymentDto {


    Long usersId;

    Long productId;

    Long remainingAmount;

//    Integer period;

    Long monthlyPayment;

    LocalDate paymentTerms;

}
