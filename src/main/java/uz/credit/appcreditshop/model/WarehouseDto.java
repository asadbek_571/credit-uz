package uz.credit.appcreditshop.model;

import lombok.*;
import lombok.experimental.FieldDefaults;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WarehouseDto {

    Long productId;

    String categoryName;

    Boolean active;

    Integer amount;


}
