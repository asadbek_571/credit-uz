package uz.credit.appcreditshop.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse<T> {

    private T data;
    private String errorMessage = "";
    private boolean success;

    public ApiResponse(T data) {
        this.data = data;
        this.errorMessage = "";
        this.success = true;
    }

    public ApiResponse(T data, boolean success) {
        this.data = data;
        this.errorMessage = "";
        this.success = success;
    }

    public ApiResponse(String errorMessage) {
        this.data = null;
        this.errorMessage = errorMessage;
        this.success = false;
    }


}
