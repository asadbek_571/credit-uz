package uz.credit.appcreditshop.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmployeeDto {

    Long userId;

//    String phoneNumber;

    List<Long> positionIds;

    Boolean active;

}
