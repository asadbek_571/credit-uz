package uz.credit.appcreditshop.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.credit.appcreditshop.entity.address.Address;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchDto {

    String name;

    Boolean active;

    Long addressId;
}
