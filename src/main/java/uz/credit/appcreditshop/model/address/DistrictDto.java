package uz.credit.appcreditshop.model.address;

import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.credit.appcreditshop.entity.address.Region;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DistrictDto {

    private String name;

    private Long regionId;
}

