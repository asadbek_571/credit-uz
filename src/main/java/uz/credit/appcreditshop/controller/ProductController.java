package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.ProductDto;
import uz.credit.appcreditshop.model.address.RegionDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.ProductService;
import uz.credit.appcreditshop.service.RegionService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final ProductService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<ProductDto>> add(@RequestBody ProductDto dto){
        return service.add(dto);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<ApiResponse<ProductDto>> update(@PathVariable(value = "id")Long id, @RequestBody ProductDto dto){
        return service.update(id,dto);
    }
}
