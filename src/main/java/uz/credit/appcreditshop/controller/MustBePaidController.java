//package uz.credit.appcreditshop.controller;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import uz.credit.appcreditshop.entity.CreditPayment;
////import uz.credit.appcreditshop.model.EmployeeDto;
//import uz.credit.appcreditshop.model.MustBePaidDto;
////import uz.credit.appcreditshop.model.MustBePaidGetDto;
//import uz.credit.appcreditshop.model.response.ApiResponse;
////import uz.credit.appcreditshop.service.EmployeeService;
////import uz.credit.appcreditshop.service.MustBePaidService;
////
////import java.time.LocalDate;
//import java.util.List;
//
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/must-be-paid")
//public class MustBePaidController {
//
////    private final MustBePaidService service;
//
//    @GetMapping("/get")
//    public ResponseEntity<ApiResponse<List<CreditPayment>>> get(@RequestBody MustBePaidGetDto dto){
//        return service.get(dto);
//    }
//
//    @PostMapping("/update/{id}")
//    public ResponseEntity<ApiResponse<MustBePaidDto>> update(@PathVariable(value = "id")Long id, @RequestBody MustBePaidDto dto){
//        return service.update(id,dto);
//    }
//}
