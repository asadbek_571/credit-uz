package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.CategoryDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.CategoryService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/category")
public class CategoryController {

    private final CategoryService categoryService;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<CategoryDto>> add(@RequestBody CategoryDto dto){
        return categoryService.add(dto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse<CategoryDto>> update(@PathVariable(value = "id")Long id, @RequestBody CategoryDto dto){
        return categoryService.update(id,dto);
    }

}
