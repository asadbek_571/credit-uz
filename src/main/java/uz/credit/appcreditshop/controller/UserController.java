package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.MustBePaidDto;
import uz.credit.appcreditshop.model.UserDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
//import uz.credit.appcreditshop.service.MustBePaidService;
import uz.credit.appcreditshop.service.UserService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<UserDto>> add(@RequestBody UserDto dto){
        return service.add(dto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse<UserDto>> update(@PathVariable(value = "id")Long id, @RequestBody UserDto dto){
        return service.update(id,dto);
    }




}
