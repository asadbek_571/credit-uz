package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.ClientDto;
import uz.credit.appcreditshop.model.address.DistrictDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.ClientService;
import uz.credit.appcreditshop.service.DistrictService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/district")
public class DistrictController {

    private final DistrictService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<DistrictDto>> add(@RequestBody DistrictDto dto){
        return service.add(dto);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<ApiResponse<DistrictDto>> update(@PathVariable(value = "id")Long id, @RequestBody DistrictDto dto){
        return service.update(id,dto);
    }
}
