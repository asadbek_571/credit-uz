package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.entity.CashPayment;
import uz.credit.appcreditshop.model.CashPaymentDto;
import uz.credit.appcreditshop.model.CreditPaymentDto;
import uz.credit.appcreditshop.model.address.AddressDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.AddressService;
import uz.credit.appcreditshop.service.CashPaymentService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/cash-payment")
public class CashPaymentController {

    private final CashPaymentService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<CashPaymentDto>> add(@RequestBody CashPaymentDto dto,@RequestParam int term){
        return service.add(dto,term);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse<CashPaymentDto>> update(@PathVariable(value = "id")Long id,@RequestBody CashPaymentDto dto){
        return service.update(id,dto);
    }
}
