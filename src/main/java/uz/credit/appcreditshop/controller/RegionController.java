package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.MustBePaidDto;
import uz.credit.appcreditshop.model.address.RegionDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
//import uz.credit.appcreditshop.service.MustBePaidService;
import uz.credit.appcreditshop.service.RegionService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/region")
public class RegionController {

    private final RegionService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<RegionDto>> add(@RequestBody RegionDto dto){
        return service.add(dto);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<ApiResponse<RegionDto>> update(@PathVariable(value = "id")Long id, @RequestBody RegionDto dto){
        return service.update(id,dto);
    }
}
