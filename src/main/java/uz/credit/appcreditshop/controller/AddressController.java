package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.address.AddressDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.AddressService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/address")
public class AddressController {

    private final AddressService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<AddressDto>> add(@RequestBody AddressDto dto){
        return service.add(dto);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse<AddressDto>> update(@PathVariable(value = "id")Long id,@RequestBody AddressDto dto){
        return service.update(id,dto);
    }
}
