package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.CategoryDto;
import uz.credit.appcreditshop.model.ClientDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.CategoryService;
import uz.credit.appcreditshop.service.ClientService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/client")
public class ClientController {

    private final ClientService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<ClientDto>> add(@RequestBody ClientDto dto){
        return service.add(dto);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<ApiResponse<ClientDto>> update(@PathVariable(value = "id")Long id, @RequestBody ClientDto dto){
        return service.update(id,dto);
    }
}
