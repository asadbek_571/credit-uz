package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.entity.Warehouse;
import uz.credit.appcreditshop.model.MustBePaidDto;
import uz.credit.appcreditshop.model.WarehouseDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
//import uz.credit.appcreditshop.service.MustBePaidService;
import uz.credit.appcreditshop.service.WarehouseService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/warehouse")
public class WarehouseController {

    private final WarehouseService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<WarehouseDto>> add(@RequestBody WarehouseDto dto){
        return service.add(dto);
    }

    @GetMapping("/getList")
    public ResponseEntity<ApiResponse<List<Warehouse>>> getList(@RequestParam(value = "productId")Long productId,
                                                                @RequestParam(defaultValue = "0",name = "page",required = false)Integer page,
                                                                @RequestParam(defaultValue = "5",name = "size",required = false)Integer size){
        return service.getList(productId,page,size);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse<WarehouseDto>> update(@PathVariable(value = "id")Long id, @RequestBody WarehouseDto dto){
        return service.update(id,dto);
    }
}
