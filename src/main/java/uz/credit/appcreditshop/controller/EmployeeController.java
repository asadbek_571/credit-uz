package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.model.EmployeeDto;
import uz.credit.appcreditshop.model.address.DistrictDto;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.EmployeeService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService service;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<EmployeeDto>> add(@RequestBody EmployeeDto dto){
        return service.add(dto);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<ApiResponse<EmployeeDto>> update(@PathVariable(value = "id")Long id, @RequestBody EmployeeDto dto){
        return service.update(id,dto);
    }
}
