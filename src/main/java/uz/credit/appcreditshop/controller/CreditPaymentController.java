package uz.credit.appcreditshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.credit.appcreditshop.entity.CreditPayment;
import uz.credit.appcreditshop.model.*;
import uz.credit.appcreditshop.model.response.ApiResponse;
import uz.credit.appcreditshop.service.CashPaymentService;
import uz.credit.appcreditshop.service.CreditPaymentService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/credit-payment")
public class CreditPaymentController {

    private final CreditPaymentService service;


    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse<CreditPaymentDto>> update(@PathVariable(value = "id")Long id,
                                                                @RequestBody CreditPaymentCrudDto dto){
        return service.update(id,dto);
    }

    @GetMapping("/get")
    public ResponseEntity<ApiResponse<List<CreditPayment>>> get(@RequestBody(required = false) CreditGetDto dto){
        return service.get(dto);
    }
}
