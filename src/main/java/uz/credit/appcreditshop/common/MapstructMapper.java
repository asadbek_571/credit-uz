package uz.credit.appcreditshop.common;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;
import uz.credit.appcreditshop.entity.*;
import uz.credit.appcreditshop.entity.address.Address;
import uz.credit.appcreditshop.model.*;
import uz.credit.appcreditshop.model.address.AddressDto;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface MapstructMapper {

    UserDto toUser(Users users);

    Users toUser(UserDto user);

    @Mapping(target = "districtId", source = "district.id")
    AddressDto toAddress(Address address);

    @Mapping(target = "district.id", source = "districtId")
    Address toAddress(AddressDto address);

    @Mapping(target = "addressId", source = "address_id")
    BranchDto toBranch(Branch branch);

    @Mapping(target = "parentCategoryId", source = "parentCategory.id")
    CategoryDto toCategory(Category category);

    @Mapping(target = "parentCategory.id", source = "parentCategoryId")
    Category toCategory(CategoryDto category);




    @Mapping(target = "productId", source = "product.id")
    DiscountDto toDiscount(Discounts discounts);

    EmployeeDto toEmployee(Employee employee);

//    @Mapping(target = "clientId", source = "users_id")
    MustBePaidDto toMustBePaid(MustBePaid mustBePaid);



    Positions toPositions(Positions positions);

    @Mapping(target = "category.id", source = "categoryId")
    Products toProducts(ProductDto dto);

    @Mapping(target = "categoryId", source = "category_id")
    ProductDto toProducts(Products products);

    @Mapping(target = "productId", source = "product.id")
    WarehouseDto toWarehouse(Warehouse warehouse);

    @Mapping(target = "product.id", source = "productId")
    Warehouse toWarehouse(WarehouseDto dto);

    @Mapping(target = "users.id", source = "usersId")
    @Mapping(target = "product.id", source = "productId")
    CreditPayment toCreditPayment(CreditPaymentDto dto);

    @Mapping(target = "users.id", source = "usersId")
    @Mapping(target = "products.id", source = "productId")
    CashPayment toCashPayment(CashPaymentDto dto);

}
