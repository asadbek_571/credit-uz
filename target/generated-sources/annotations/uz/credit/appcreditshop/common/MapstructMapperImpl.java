package uz.credit.appcreditshop.common;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import uz.credit.appcreditshop.entity.Branch;
import uz.credit.appcreditshop.entity.CashPayment;
import uz.credit.appcreditshop.entity.Category;
import uz.credit.appcreditshop.entity.CreditPayment;
import uz.credit.appcreditshop.entity.Discounts;
import uz.credit.appcreditshop.entity.Employee;
import uz.credit.appcreditshop.entity.MustBePaid;
import uz.credit.appcreditshop.entity.Positions;
import uz.credit.appcreditshop.entity.Products;
import uz.credit.appcreditshop.entity.Users;
import uz.credit.appcreditshop.entity.Warehouse;
import uz.credit.appcreditshop.entity.address.Address;
import uz.credit.appcreditshop.entity.address.District;
import uz.credit.appcreditshop.model.BranchDto;
import uz.credit.appcreditshop.model.CashPaymentDto;
import uz.credit.appcreditshop.model.CategoryDto;
import uz.credit.appcreditshop.model.CreditPaymentDto;
import uz.credit.appcreditshop.model.DiscountDto;
import uz.credit.appcreditshop.model.EmployeeDto;
import uz.credit.appcreditshop.model.MustBePaidDto;
import uz.credit.appcreditshop.model.ProductDto;
import uz.credit.appcreditshop.model.UserDto;
import uz.credit.appcreditshop.model.WarehouseDto;
import uz.credit.appcreditshop.model.address.AddressDto;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-08-30T08:50:46+0500",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_302 (Amazon.com Inc.)"
)
@Component
public class MapstructMapperImpl implements MapstructMapper {

    @Override
    public UserDto toUser(Users users) {
        if ( users == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setFirstName( users.getFirstName() );
        userDto.setLastName( users.getLastName() );
        if ( users.getBirthday() != null ) {
            userDto.setBirthday( DateTimeFormatter.ISO_LOCAL_DATE.format( users.getBirthday() ) );
        }
        userDto.setPassportSerial( users.getPassportSerial() );
        userDto.setPhoneNumber( users.getPhoneNumber() );
        userDto.setAddress( users.getAddress() );

        return userDto;
    }

    @Override
    public Users toUser(UserDto user) {
        if ( user == null ) {
            return null;
        }

        Users users = new Users();

        users.setFirstName( user.getFirstName() );
        users.setLastName( user.getLastName() );
        if ( user.getBirthday() != null ) {
            users.setBirthday( LocalDate.parse( user.getBirthday() ) );
        }
        users.setPassportSerial( user.getPassportSerial() );
        users.setPhoneNumber( user.getPhoneNumber() );
        users.setAddress( user.getAddress() );

        return users;
    }

    @Override
    public AddressDto toAddress(Address address) {
        if ( address == null ) {
            return null;
        }

        AddressDto addressDto = new AddressDto();

        addressDto.setDistrictId( addressDistrictId( address ) );
        addressDto.setStreet( address.getStreet() );
        addressDto.setHomeNumber( address.getHomeNumber() );

        return addressDto;
    }

    @Override
    public Address toAddress(AddressDto address) {
        if ( address == null ) {
            return null;
        }

        Address address1 = new Address();

        address1.setDistrict( addressDtoToDistrict( address ) );
        address1.setStreet( address.getStreet() );
        address1.setHomeNumber( address.getHomeNumber() );

        return address1;
    }

    @Override
    public BranchDto toBranch(Branch branch) {
        if ( branch == null ) {
            return null;
        }

        BranchDto branchDto = new BranchDto();

        branchDto.setAddressId( branch.getAddress_id() );
        branchDto.setName( branch.getName() );
        branchDto.setActive( branch.getActive() );

        return branchDto;
    }

    @Override
    public CategoryDto toCategory(Category category) {
        if ( category == null ) {
            return null;
        }

        CategoryDto categoryDto = new CategoryDto();

        categoryDto.setParentCategoryId( categoryParentCategoryId( category ) );
        categoryDto.setName( category.getName() );

        return categoryDto;
    }

    @Override
    public Category toCategory(CategoryDto category) {
        if ( category == null ) {
            return null;
        }

        Category category1 = new Category();

        category1.setParentCategory( categoryDtoToCategory( category ) );
        category1.setName( category.getName() );

        return category1;
    }

    @Override
    public DiscountDto toDiscount(Discounts discounts) {
        if ( discounts == null ) {
            return null;
        }

        DiscountDto discountDto = new DiscountDto();

        discountDto.setProductId( discountsProductId( discounts ) );
        discountDto.setCurrentPrice( discounts.getCurrentPrice() );

        return discountDto;
    }

    @Override
    public EmployeeDto toEmployee(Employee employee) {
        if ( employee == null ) {
            return null;
        }

        EmployeeDto employeeDto = new EmployeeDto();

        employeeDto.setActive( employee.getActive() );

        return employeeDto;
    }

    @Override
    public MustBePaidDto toMustBePaid(MustBePaid mustBePaid) {
        if ( mustBePaid == null ) {
            return null;
        }

        MustBePaidDto mustBePaidDto = new MustBePaidDto();

        return mustBePaidDto;
    }

    @Override
    public Positions toPositions(Positions positions) {
        if ( positions == null ) {
            return null;
        }

        Positions positions1 = new Positions();

        positions1.setId( positions.getId() );
        positions1.setPosition( positions.getPosition() );

        return positions1;
    }

    @Override
    public Products toProducts(ProductDto dto) {
        if ( dto == null ) {
            return null;
        }

        Products products = new Products();

        products.setCategory( productDtoToCategory( dto ) );
        products.setPrice( dto.getPrice() );

        return products;
    }

    @Override
    public ProductDto toProducts(Products products) {
        if ( products == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        productDto.setCategoryId( products.getCategory_id() );
        productDto.setPrice( products.getPrice() );

        return productDto;
    }

    @Override
    public WarehouseDto toWarehouse(Warehouse warehouse) {
        if ( warehouse == null ) {
            return null;
        }

        WarehouseDto warehouseDto = new WarehouseDto();

        warehouseDto.setProductId( warehouseProductId( warehouse ) );
        warehouseDto.setCategoryName( warehouse.getCategoryName() );
        warehouseDto.setActive( warehouse.getActive() );
        warehouseDto.setAmount( warehouse.getAmount() );

        return warehouseDto;
    }

    @Override
    public Warehouse toWarehouse(WarehouseDto dto) {
        if ( dto == null ) {
            return null;
        }

        Warehouse warehouse = new Warehouse();

        warehouse.setProduct( warehouseDtoToProducts( dto ) );
        warehouse.setActive( dto.getActive() );
        warehouse.setCategoryName( dto.getCategoryName() );
        warehouse.setAmount( dto.getAmount() );

        return warehouse;
    }

    @Override
    public CreditPayment toCreditPayment(CreditPaymentDto dto) {
        if ( dto == null ) {
            return null;
        }

        CreditPayment creditPayment = new CreditPayment();

        creditPayment.setUsers( creditPaymentDtoToUsers( dto ) );
        creditPayment.setProduct( creditPaymentDtoToProducts( dto ) );
        creditPayment.setRemainingAmount( dto.getRemainingAmount() );
        creditPayment.setMonthlyPayment( dto.getMonthlyPayment() );
        creditPayment.setPaymentTerms( dto.getPaymentTerms() );

        return creditPayment;
    }

    @Override
    public CashPayment toCashPayment(CashPaymentDto dto) {
        if ( dto == null ) {
            return null;
        }

        CashPayment cashPayment = new CashPayment();

        cashPayment.setUsers( cashPaymentDtoToUsers( dto ) );
        cashPayment.setProducts( cashPaymentDtoToProducts( dto ) );
        if ( dto.getPayment() != null ) {
            cashPayment.setPayment( dto.getPayment().doubleValue() );
        }

        return cashPayment;
    }

    private Long addressDistrictId(Address address) {
        if ( address == null ) {
            return null;
        }
        District district = address.getDistrict();
        if ( district == null ) {
            return null;
        }
        Long id = district.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    protected District addressDtoToDistrict(AddressDto addressDto) {
        if ( addressDto == null ) {
            return null;
        }

        District district = new District();

        district.setId( addressDto.getDistrictId() );

        return district;
    }

    private Long categoryParentCategoryId(Category category) {
        if ( category == null ) {
            return null;
        }
        Category parentCategory = category.getParentCategory();
        if ( parentCategory == null ) {
            return null;
        }
        Long id = parentCategory.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    protected Category categoryDtoToCategory(CategoryDto categoryDto) {
        if ( categoryDto == null ) {
            return null;
        }

        Category category = new Category();

        category.setId( categoryDto.getParentCategoryId() );

        return category;
    }

    private Long discountsProductId(Discounts discounts) {
        if ( discounts == null ) {
            return null;
        }
        Products product = discounts.getProduct();
        if ( product == null ) {
            return null;
        }
        Long id = product.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    protected Category productDtoToCategory(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }

        Category category = new Category();

        category.setId( productDto.getCategoryId() );

        return category;
    }

    private Long warehouseProductId(Warehouse warehouse) {
        if ( warehouse == null ) {
            return null;
        }
        Products product = warehouse.getProduct();
        if ( product == null ) {
            return null;
        }
        Long id = product.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    protected Products warehouseDtoToProducts(WarehouseDto warehouseDto) {
        if ( warehouseDto == null ) {
            return null;
        }

        Products products = new Products();

        products.setId( warehouseDto.getProductId() );

        return products;
    }

    protected Users creditPaymentDtoToUsers(CreditPaymentDto creditPaymentDto) {
        if ( creditPaymentDto == null ) {
            return null;
        }

        Users users = new Users();

        users.setId( creditPaymentDto.getUsersId() );

        return users;
    }

    protected Products creditPaymentDtoToProducts(CreditPaymentDto creditPaymentDto) {
        if ( creditPaymentDto == null ) {
            return null;
        }

        Products products = new Products();

        products.setId( creditPaymentDto.getProductId() );

        return products;
    }

    protected Users cashPaymentDtoToUsers(CashPaymentDto cashPaymentDto) {
        if ( cashPaymentDto == null ) {
            return null;
        }

        Users users = new Users();

        users.setId( cashPaymentDto.getUsersId() );

        return users;
    }

    protected Products cashPaymentDtoToProducts(CashPaymentDto cashPaymentDto) {
        if ( cashPaymentDto == null ) {
            return null;
        }

        Products products = new Products();

        products.setId( cashPaymentDto.getProductId() );

        return products;
    }
}
